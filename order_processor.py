#!/bin/python3

# db = boto3.resource('dynamodb', 'us-east-1')
# Table = db.Table('test')
# EntryQueue = "https://queue.amazonaws.com/833087542003/q1"

import boto3
import time, json, datetime
from random import randint

class Orders(object):
    """Creates New order in db and send tasks to the EntryQueue
       Need to be initiated with the TABLE, REGION and ENTRYQUEUE
       A order queue must be set
    """
    sqs = boto3.client("sqs")
    db = ""
    Table = ""
    EntryQueue = [ i for i in sqs.list_queues()["QueueUrls"] if "order" in i ][0]
    Queues = [ i for i in sqs.list_queues()["QueueUrls"] if not "order" in i ]

    def __init__(self, TABLE, REGION):
        self.TABLE = TABLE
        self.REGION = REGION
        self.updateVars(TABLE, REGION)

    def updateVars(cls, TABLE, REGION):
        cls.db = boto3.resource('dynamodb', REGION)
        cls.Table = cls.db.Table(TABLE)

    @staticmethod
    def Test_msg():
        R1 = str(randint(10,20)) 
        R2 = str(randint(10,20)) 
        Test_msg = json.loads(f'{{"NEW_ORDER": "{R1}", "TABLE":{R2}, "ORDER":[1,2,99]}}') #double bracket as scape
        return Test_msg

    @classmethod
    def getMsg(self, queue_url, rm=False):
        """Get message and deletes it when received"""
        messages = []
        while True:
            msg = self.sqs.receive_message(
                QueueUrl=queue_url,
                AttributeNames=['all'])

            try:
                messages += msg['Messages'] 

            except KeyError:
                break

            if rm:
                entries = [
                    {'Id':            msg['MessageId'],
                     'ReceiptHandle': msg['ReceiptHandle']}
                    for msg in msg['Messages']
                ]
                resp = self.sqs.delete_message_batch(
                    QueueUrl=queue_url, Entries=entries
                )

                # deleting handling error
                if len(resp['Successful']) != len(entries):
                    raise RuntimeError(
                        f"Failed to delete messages: entries={entries!r} resp={resp!r}"
                    )
                print(resp)

        return messages

    @classmethod
    def sendMsg(self, message, queue_url):
        """Send messages and update db
           Json message must look:
           {"NEW_ORDER":1,"TABLE":1,"ORDER":[1,2,3],"COMMENT":''}
        """
        if isinstance(message, dict):
            message = json.dumps(message)

        test = self.Test_msg
        #queue_url = self.EntryQueue
        send_msg = self.sqs.send_message(
            QueueUrl=queue_url,
            MessageBody=message,
        )

        if send_msg["ResponseMetadata"]["HTTPStatusCode"] == 200:
            return True

        return None

    #@classmethod
    def pullNewOrder(self):
        """Pulls NEW_ORDER's queue and updates db
           Json message must look:
           {"NEW_ORDER":1,"TABLE":1,"ORDER":[1,2,3],"COMMENT":''}
        """
        Table = self.db.Table(self.TABLE)
        msg = self.getMsg(rm=True,queue_url=self.EntryQueue)

        if msg:
            DATE = str(datetime.date.today())
            orders = [ json.loads(i['Body']) for i in msg ]
            orders = [ dict(i, **{'STATE': "NEW","DATE": DATE}) for i in orders ]       ## adds the first state
            orders = [ dict(i, **{'NEW_ORDER': str(i["NEW_ORDER"]) }) for i in orders ] ## adds the first state
            for order in orders:
               for k,v in order.items():
                  if isinstance(v, list):
                     order["ORDER"] = [int(i) for i in order["ORDER"]]

            for i in orders:
                Table.put_item(Item={**i})

            return orders

        return None

    def setOrderState(self, identity):
        """Set state as per the state on db
           Json message must look:
           {"NEW_ORDER":1,"TABLE":1,"ORDER":[1,2,3],"COMMENT":''}
        """

        from boto3.dynamodb.conditions import Key,Attr

        queue_map = {"Q1": [i for i in self.Queues if "q1" in i][0],
                     "Q2": [i for i in self.Queues if "q2" in i][0]}


        orders_per_state = {"NEW":[], "Q1": [], "Q2": []}
        orders = self.Table.scan()["Items"]

        if orders:
            # CLEAN DATA
            for order in orders:
               for k,v in order.items():
                  if k == "ORDER":
                     order["ORDER"] = [int(i) for i in order["ORDER"]]
                  if k == "TABLE":
                     order["TABLE"] = int(v)

            # POPULATE ORDER_PER_STATE
            for order in orders:
                if order["STATE"] == "NEW":
                    orders_per_state["NEW"].append(order)
                if order["STATE"] == "Q1":
                    orders_per_state["Q1"].append(order)
                if order["STATE"] == "Q2":
                    orders_per_state["Q2"].append(order)

            # todo : add validations after every step
            for state in orders_per_state.keys():
                for order in orders_per_state[state]:
                    if order["STATE"] == "NEW" and identity == "waiter":
                        order["STATE"] = "Q1"
                        self.Table.put_item(Item={**order})
                        self.sendMsg(order,queue_url=queue_map["Q1"] )
                        #self.getMsg(rm=True,queue_url=self.Queues[0])
                    elif order["STATE"] == "Q1" and identity == "cooker":
                        order["STATE"] = "Q2"
                        self.Table.put_item(Item={**order})
                        self.sendMsg(order,queue_url=queue_map["Q2"] )
                        self.getMsg(rm=True,queue_url=self.Queues[0]) # once taken deletes it

                    elif order["STATE"] == "Q2" and identity == "waiter":
                        order["STATE"] = "DONE"
                        self.Table.put_item(Item={**order})
                        self.getMsg(rm=True,queue_url=self.Queues[1]) # once taken deletes it
                    elif order["STATE"] == "DONE":
                        pass

        return None


